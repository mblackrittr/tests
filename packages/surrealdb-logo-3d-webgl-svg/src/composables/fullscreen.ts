// these APIs are auto-imported from @vueuse/core
export const fullscreen = {
  element: ref<HTMLElement | null>(null),
  context: null as any,
}
export function toggleFullscreen() {
  if (!fullscreen.context) {
    const { toggle } = useFullscreen(fullscreen.element)
    fullscreen.context = toggle
  }

  fullscreen.context()
}
