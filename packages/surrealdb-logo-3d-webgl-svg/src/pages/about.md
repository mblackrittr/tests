---
title: About
---

<div class="text-center">
  <!-- You can use Vue components inside markdown -->
  <div i-carbon-dicom-overlay class="text-4xl -mb-6 m-auto" />
  <h3>About</h3>
</div>

# SurrealDB Logo in 3D WebGL from SVG with ThreeJS

> Using Vue, TresJS and even some Effect-TS.



<div>
  Generating the 3D model on-the-fly using ThreeJS' extrusion and beveling of this SVG:
  <img src="/media/surrealdb-icon.svg" mt--4 style="width: 25%" />
</div>


Check out the [GitLab repo](https://gitlab.com/mblackrittr/tests/tree/master/packages/surrealdb-logo-3d-webgl-svg/) for the code.


## Code Excerpts

### Extrusion of Flat SVG Data to 3D Model

```ts
const shapes = svgData.paths[0].toShapes(false)

// z!Black | DODO (2024-06-06 03:14)
// Check for a solution to get sane `face normals`` for `smooth shading`
// Setup again what we'd that worked, but that also smoothed the fronts!
// Open some issue(s) with ThreeJS or who's doing `ExtrudeGeometry`, etc.
const geometry = new ExtrudeGeometry(shapes, {
  steps: 1,
  curveSegments: 1,
  depth: 10,
  bevelEnabled: true,
  bevelThickness: 2.0,
  bevelSize: 1.75,
  bevelSegments: 4,
  bevelOffset: -1.5,
})

const mesh = new Mesh(geometry, material)

svgGroup.add(mesh)
```


### Use of Effect

Using [Effect-TS] by [creating an effect from callback][effect-ts-create-from-callback] to handle the rather legacy style callback of `RGBELoader().load()` and turn it into a clean `promise/await`.

```ts
// Using Effect - the glue of god - to clean the loader's "callback hell"
function loadRgbeEffect(filename: string) {
  return Effect.async<DataTexture, unknown>((resume) => {
    new RGBELoader().load(filename, (data) => {
      resume(Effect.succeed(data))
    }, undefined,
    // z!Black | DODO (2024-06-06 05:17)
    // Give some cleaner/named error here!
    (error) => {
      resume(Effect.fail(error))
    })
  })
}
```


### Generating Gradient Texture

```ts
function generateGradientTexture() {
  const size = 512

  const canvas = document.createElement('canvas')
  canvas.width = size
  canvas.height = size

  const context = canvas.getContext('2d')

  if (context) {
    context.rect(0, 0, size, size)
    const gradient = context.createLinearGradient(512, 0, 0, size)
    gradient.addColorStop(0, COLOR_PINK)
    gradient.addColorStop(0.75, COLOR_VIOLET)
    context.fillStyle = gradient
    context.fill()
  }

  return canvas
}
```

<!-- urls -->
[surrealdb-logo-3d-demo]: https://surrealdblogo.netlify.app

[effect-ts]: https://effect.website
[effect-ts-create-from-callback]: https://effect.website/docs/guides/essentials/creating-effects#from-a-callback
